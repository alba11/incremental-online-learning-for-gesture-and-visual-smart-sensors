import os, sys
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import pandas as pd
import numpy as np
import re
import random
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import optimizers
from PIL import Image
from sklearn.metrics import confusion_matrix
import seaborn as sns

ROOT_PATH = os.path.abspath('')
#sys.path.insert(0, ROOT_PATH + '/lib')

import lib.myLib_barChart as myBar
import lib.myLib_confMatrix as myMatrix
import lib.myLib_parseData as myParse
import lib.myLib_pieChart as myPie
import lib.myLib_table as myTable
import lib.myLib_testModel as myTest
from lib.myLib_testModel import letterToSoftmax
import lib.myLib_writeFile as myWrite
import lib.myLib_debugFiles as myDebug

np.set_printoptions(suppress=True)

# Definition of string values for the bold print
S_BOLD = '\033[1m'
E_BOLD = '\033[0m'

DEBUG_HISTORY = 0      # 0 for no debug          | 1 for yes debug

vowels_data, vowels_label = myParse.loadDataFromTxt('vowels_OL')
OL_data_train_vow, OL_label_train_vow, OL_data_test_vow, OL_label_test_vow = myParse.parseTrainTest(vowels_data, vowels_label, 0.7)

data, label = myParse.loadDataFromTxt('training_file_0')
data_train, label_train, data_test, label_test = myParse.parseTrainTest(data, label, 0.81)

print('\nTRAIN DATASET SANITY CHECK')
myParse.sanityCheckDataset(label_train)
print('\nTEST DATASET SANITY CHECK')
myParse.sanityCheckDataset(label_test)

MODEL_PATH = ROOT_PATH + "/Saved_models/"

#model = keras.models.load_model(MODEL_PATH + 'Original_model/model.h5')
model = keras.models.load_model(MODEL_PATH + 'MODELS_OF_PRESENTAITON/Original_model - Copia/model.h5')

def myFunc_softmax(array):
    """ Computes softmax of an array

    Parameters
    ----------
    array : array_like
        Is the array of which I want to compute the softmax operation

    Returns
    -------
    ret_ary : array_like
        Softmax output
    """

    if (len(array.shape) == 2):
        array = array[0]

    size = len(array)
    ret_ary = np.zeros([len(array)])
    m = array[0]
    sum_val = 0

    for i in range(0, size):
        if (m < array[i]):
            m = array[i]

    for i in range(0, size):
        sum_val += np.exp(array[i] - m)

    constant = m + np.log(sum_val)
    for i in range(0, size):
        ret_ary[i] = np.exp(array[i] - constant)

    return ret_ary


class Custom_Layer(object):
    def __init__(self, model):
        # Related to the layer
        self.ML_frozen = keras.models.Sequential(model.layers[:-1])  # extract the last layer from the original model
        self.ML_frozen.compile()

        self.W = np.array(model.layers[-1].get_weights()[0])  # extract the weights from the last layer
        self.b = np.array(model.layers[-1].get_weights()[1])  # extract the biases from the last layer

        self.W_2 = np.zeros(self.W.shape)
        self.b_2 = np.zeros(self.b.shape)

        self.label = ['A', 'E', 'I', 'O', 'U']  # the original model knows only the vowels
        self.std_label = ['A', 'E', 'I', 'O', 'U', 'B', 'R', 'M']

        self.l_rate = 0  # learning rate that changes depending on the algorithm

        self.batch_size = 0

        # Related to the results fo the model
        self.conf_matr = np.zeros((8, 8))  # container for the confusion matrix
        self.macro_avrg_precision = 0
        self.macro_avrg_recall = 0
        self.macro_avrg_F1score = 0

        self.title = ''  # title that will be displayed on plots
        self.filename = ''  # name of the files to be saved (plots, charts, conf matrix)

    # Function that is used for the prediction of the model saved in this class
    def predict(self, x):
        mat_prod = np.array(np.matmul(x, self.W) + self.b)
        return myFunc_softmax(mat_prod)  # other method -> use kera and change np.array() with tf.nn.softmax(mat_prod)


def checkLabelKnown(model, current_label):
    found = 0

    for i in range(0, len(model.label)):
        if (current_label == model.label[i]):
            found = 1

    # If the label is not known
    if (found == 0):
        print(f'\n\n    New letter detected -> letter \033[1m{current_label}\033[0m \n')

        model.label.append(current_label)  # Add new letter to label

        # Increase weights and biases dimensions
        model.W = np.hstack((model.W, np.zeros([128, 1])))  # width = 5, height = 128
        model.b = np.hstack((model.b, np.zeros([1])))

        model.W_2 = np.hstack((model.W_2, np.zeros([128, 1])))
        model.b_2 = np.hstack((model.b_2, np.zeros([1])))

if(DEBUG_HISTORY == 1):
    numb = data_train.shape[0]

    frozenOut_pc  = np.zeros((numb, 128))
    weight_pc     = np.zeros((numb, 80))
    bias_pc       = np.zeros((numb,8))
    preSoftmax_pc = np.zeros((numb,8))
    softmax_pc    = np.zeros((numb,8))

    weight_letter_b = np.zeros((128, numb))

    selected_w = [46,13,107,3,57,65,127,81,89,70,
                    143,239,142,158,207,189,172,230,156,208,
                    374,359,375,371,303,298,350,257,349,333,
                    402,502,485,461,489,479,454,508,485,480,
                    527,565,614,517,528,613,625,623,587,521,
                    712,742,685,746,759,747,754,702,653,640,
                    775,809,798,853,804,840,828,788,890,819,
                    906,1019,911,1005,1016,953,1016,987,961,1023]

def printEvalMetrics(model, x_train, x_test, y_train, y_test):

    # this function uses both train and test dataset to evaluate the OL method
    standard_label = ['A','E','I','O','U','B','R','M'] # order of labels that is used in all plots
    print("\n*** Printing evaluation metrics ***")
    cntr = 1
    learn_rate = model.l_rate
    train_samples = x_train.shape[0]
    test_samples = x_test.shape[0]
    tot_samples = train_samples + test_samples

    # PRINT OVERALL TRAINING PERFORMANCE METRICS
    pred_label = []
    # Cycle over all samples
    for i in range(0, tot_samples):
        if i < train_samples:
            current_label = y_train[i]
        else:
            current_label = y_test[i - train_samples]

        checkLabelKnown(model, current_label)
        y_true_soft = letterToSoftmax(current_label, model.label)

        # PPREDICTION

        # PPREDICTION
        if (i < train_samples):
            y_ML = model.ML_frozen.predict(x_train[i, :].reshape(1, x_train.shape[1]))

            if (DEBUG_HISTORY == 1):
                temp = np.copy(np.array(np.matmul(y_ML, model.W) + model.b))
                temp = temp[0]
        else:
            y_ML = model.ML_frozen.predict(x_test[i - train_samples, :].reshape(1, x_test.shape[1]))
        y_pred = model.predict(y_ML[0, :])
        pred_label.append(y_pred)

    #pred_label = model.predict(x_test)
    y_all = np.concatenate((y_train, y_test))
    true_label = myTest.letterToSoft_all(y_all, standard_label)
    pred_label = np.argmax(pred_label, axis=1)
    true_label = np.argmax(true_label, axis=1)

    # importing confusion matrix
    from sklearn.metrics import confusion_matrix

    confusion = confusion_matrix(true_label, pred_label)
    print('Confusion Matrix\n')
    print(confusion)

    # importing accuracy_score, precision_score, recall_score, f1_score
    from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

    print('\nAccuracy: {:.2f}\n'.format(accuracy_score(true_label, pred_label)))

    print('Micro Precision: {:.2f}'.format(precision_score(true_label, pred_label, average='micro')))
    print('Micro Recall: {:.2f}'.format(recall_score(true_label, pred_label, average='micro')))
    print('Micro F1-score: {:.2f}\n'.format(f1_score(true_label, pred_label, average='micro')))

    print('Macro Precision: {:.2f}'.format(precision_score(true_label, pred_label, average='macro')))
    print('Macro Recall: {:.2f}'.format(recall_score(true_label, pred_label, average='macro')))
    print('Macro F1-score: {:.2f}\n'.format(f1_score(true_label, pred_label, average='macro')))

    print('Weighted Precision: {:.2f}'.format(precision_score(true_label, pred_label, average='weighted')))
    print('Weighted Recall: {:.2f}'.format(recall_score(true_label, pred_label, average='weighted')))
    print('Weighted F1-score: {:.2f}'.format(f1_score(true_label, pred_label, average='weighted')))

    from sklearn.metrics import classification_report

    print('\nClassification Report\n')
    print(classification_report(true_label, pred_label, target_names=['A', 'E', 'I', 'O', 'U']))

def trainOneEpoch_OL(model, x_train, x_test, y_train, y_test):
    print('**********************************\nPerforming training with OL METHOD - STOCHASTIC\n')

    cntr = 1
    learn_rate = model.l_rate
    train_samples = x_train.shape[0]
    test_samples = x_test.shape[0]
    tot_samples = train_samples + test_samples

    # Cycle over all samples
    for i in range(0, tot_samples):

        if (i < train_samples):
            current_label = y_train[i]
        else:
            current_label = y_test[i - train_samples]

        checkLabelKnown(model, current_label)
        y_true_soft = letterToSoftmax(current_label, model.label)

        # PPREDICTION
        if (i < train_samples):
            y_ML = model.ML_frozen.predict(x_train[i, :].reshape(1, x_train.shape[1]))

            if (DEBUG_HISTORY == 1):
                temp = np.copy(np.array(np.matmul(y_ML, model.W) + model.b))
                temp = temp[0]
        else:
            y_ML = model.ML_frozen.predict(x_test[i - train_samples, :].reshape(1, x_test.shape[1]))
        y_pred = model.predict(y_ML[0, :])

        # BACKPROPAGATION
        cost = y_pred - y_true_soft

        for j in range(0, model.W.shape[0]):
            # Update weights
            deltaW = np.multiply(cost, y_ML[0, j])
            dW = np.multiply(deltaW, learn_rate)
            model.W[j, :] = model.W[j, :] - dW

        # Update biases
        db = np.multiply(cost, learn_rate)
        model.b = model.b - db

        # SAVE THE WEIGHTS IN A MATRIX
        if (DEBUG_HISTORY == 1):
            if (i < numb):

                frozenOut_pc[i, :] = y_ML[0, :]

                for q in range(0, 8):
                    if (q < model.W.shape[1]):
                        bias_pc[i, q] = np.copy(model.b[q])
                        softmax_pc[i, q] = np.copy(y_pred[q])
                        preSoftmax_pc[i, q] = np.copy(temp[q])

                for q in range(0, 80):
                    if (int(selected_w[q] / 128) < model.W.shape[1]):
                        weight_pc[i, q] = np.copy(model.W[selected_w[q] % 128, int(selected_w[q] / 128)])
        # *********************************

        # if the train data is finished still train the model but save the results
        if (i >= train_samples):

            # Find the max iter for both true label and prediction
            if (np.amax(y_true_soft) != 0):
                max_i_true = np.argmax(y_true_soft)

            if (np.amax(y_pred) != 0):
                max_i_pred = np.argmax(y_pred)

            # Fill up the confusion matrix
            for k in range(0, len(model.label)):
                if (model.label[max_i_pred] == model.std_label[k]):
                    p = np.copy(k)
                if (model.label[max_i_true] == model.std_label[k]):
                    t = np.copy(k)

            model.conf_matr[t, p] += 1

        print(f"\r    Currently at {np.round(np.round(cntr / tot_samples, 4) * 100, 2)}% of dataset", end="")
        cntr += 1

batch_size_OL = 16

# DEFINE WHICH TRAINING AND PLOTS TO SHOW

KERAS      = 1
OL_vowels  = 1
OL         = 1
OL_mini    = 1
LWF        = 1
LWF_mini   = 1
OL_v2      = 1
OL_v2_mini = 1
CWR        = 1

if (KERAS == 1):
    Model_KERAS = Custom_Layer(model)
    Model_KERAS.title = 'KERAS'
    Model_KERAS.filename = 'KERAS'
    Model_KERAS.label = ['A', 'E', 'I', 'O', 'U', 'B', 'R', 'M']
    Model_KERAS.batch_size = batch_size_OL
    # DO NOT PERFORM TRAINING, KEEP IT AS IT IS, IT'S THE ORIGINAL MODEL

    myTest.test_OLlayer(Model_KERAS, model, OL_data_train_vow, OL_label_train_vow)
    myBar.plot_barChart(Model_KERAS)
    myMatrix.plot_confMatrix(Model_KERAS)
    myTable.table_params(Model_KERAS)

if (OL_vowels == 1):
    Model_OL_vowels = Custom_Layer(model)
    Model_OL_vowels.title = 'VOWELS'
    Model_OL_vowels.filename = 'OL_vowels'
    Model_OL_vowels.l_rate = 0.000005
    Model_OL_vowels.batch_size = batch_size_OL

    trainOneEpoch_OL(Model_OL_vowels, OL_data_train_vow, OL_data_test_vow, OL_label_train_vow, OL_label_test_vow)
    printEvalMetrics(Model_OL_vowels, OL_data_train_vow, OL_data_test_vow, OL_label_train_vow, OL_label_test_vow)
    myWrite.save_PCconfMatrix(Model_OL_vowels)
    #myBar.plot_barChart(Model_OL_vowels)
    #myMatrix.plot_confMatrix(Model_OL_vowels)
    #myTable.table_params(Model_OL_vowels)

